program files_example;
uses
  sysutils;
const
	text_filename = 'text-file'; // Имя текстового файла
	text_filename2 = 'text-files-example.pas';
	N = 100;
var
  fr: file of real;
  tf: text;
  f1: file;

  i,j,l: integer;
  s: string;

  ss: array [1..N] of string;
begin
randomize;

assign(tf, text_filename);	// Связываем файловую переменную с файлом
rewrite(tf);				// Открываем файл для чтения

// Запись в текстовый файл
repeat
  readln(s);				// чтение с клавиатуры
  writeln(tf,s);			// запись в файл
until s = '';
close(tf);

// Чтение из текстового файла
assign(tf, text_filename2);
reset(tf);
i:=1;
while not EOF(tf) do // Пока не достингнут конец файла (End Of File) 
  // Записываем строки в массив.
  // Стоит отметить, что содержимое файла может и не поместиться в массив.
  // Кроме того, строка в файле может быть длинее максимально допустимой для типа string
  begin readln(tf, s);
  l:=pos('//', s);
	if l <> 0 then begin
	delete(s,1,l-1);
	writeln(s);
	ss[i]:=s; inc(i); end; end; 
close(tf);

writeln;
writeln('Файл ',text_filename2,' содержит ',i,' строк(и) содержащих подстроку "//"');

// Существует ли файл?
writeln('Введите имя файла');
readln(s);
// Способ 1: функция из библиотеки SysUtils
if FileExists(s) then writeln('файл "',s,'" существует')
	else writeln('файл "',s,'" НЕ существует');

// Способ 2: с помощью проверки статуса операции ввода-вывода.
// необходимо отключить контроль операций ввода-вывода с помощью директив компилятора
// иначе попытка открыть несуществующий файл для чтения приведёт к аварийному завершению программы
assign(tf, s);
{$I-}
reset(tf); 		// Пробуем открыть файл для чтения
{$I+}
// Если в функция IOresult вернёт 0, то файл существует.
if IOresult=0 then writeln('файл "',s,'" существует')
	else writeln('файл "',s,'" НЕ существует');
end.

//123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123=======