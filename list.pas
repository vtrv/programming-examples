﻿program list_example;
type
	ListElement = record
		x: Real;				// Вещественное число
		next: ^ListElement;		// Указатель на запись ListElement
	end;
var
	First, cur, e: ^ListElement;	// Переменные типа "Указатель на запись ListElement"
									// First - указатель на начало ("голову") списка
									// указатель cur будет играть роль текущего элемента списка
									// (по аналогии с i-м элементом массива)
									// e - временный указатель.
	i,j,n: integer;
begin
	n:=10;
	randomize;

	new(First);				// Выделим память для записи ListElement и запишем в переменную First адрес
							// по которому выделена память

	First^.next := nil;		// Указатель на следующий элемент пока пустой (nil)
	First^.x := random;		// Чтобы обратится непосредственно к содержимому памяти (здесь это запись ListElement),
							// а не указателю на неё используем ^ после имени указателя. 
							// Далее с помощью оператора . обратимся к полю записи.
	cur := First;			

	for i:=2 to N do
	  begin
	  	new(e);				// Выделим память для ещё одной записи и запишём её адрес в переменную e
	  	e^.x := random;		// Запишем данные в выделенную память
	  	e^.next := nil;	

	  	cur^.next := e;		// Запишем в имеющийся элемент списка указатель на вновь созданный
	  	cur := e;

	  end;



	// Переберём весь список напечатав содержимое его элементов (поля х)
	writeln('Элементы списка:');
	cur := First;
	while cur^.next <> nil do
	  begin
	    write(cur^.x:5:2);
	    cur := cur^.next;
	  end;

	 writeln;

	// Перейти к i-му элементу списка
	i := 3;
	cur := First;
	j := 0;
	while (cur^.next <> nil) and (j<i) do
	  begin
	    cur := cur^.next;
	    inc(j);
	  end;	
	writeln(i,'-й элемент списка ', cur^.x:5:2);

	// Очистим выделенную память
	writeln('Элементы списка в порядке их удаления: ');
	cur := First;
	while cur^.next <> nil do
	begin
	e := cur;
	write(e^.x:5:2);		// Для того чтобы удостоверится в том, что освобождена вся память
							// будем выводить содержимое каждого элемента списка перед его удалением
	cur := cur^.next;
	dispose(e);
	end;
	writeln;
end.
