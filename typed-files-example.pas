program typed_files;
var 
  bf: file of integer;
  tf: text;

  i,n: integer;
  x: integer;
begin
	randomize;
	// Сохраним массив целых чисел в текстовый и бинарный файл а затем сравним размер этих файлов.
	repeat
	writeln('Сколько случайных чисел сгенерировать?');
	readln(n);
	until n > 0;

	assign(bf, 'typed-file.bin');
	assign(tf, 'text-file.txt');
	rewrite(bf);
	rewrite(tf);

	// Запишем число 255 (FF00)
	x:=255;
	write(x:6); 
	write(bf,x);

	for i:=1 to n-1 do begin
	x:=random(maxint);
	if i<=10 then write(x:6); 
	write(bf,x);
	writeln(tf,x);
	end;
	if i>10 then writeln(' ...');

	close(bf);
	close(tf);
end.