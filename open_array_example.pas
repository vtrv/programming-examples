program open_array_example;
const N = 10;

type Matrix = array of array of Real;
	 Vector = array of Real;

procedure print_vector(const v: array of Real);
var i: word;
begin
	writeln('print_vector. Мин. и макс. индексы массива: ', low(v),', ' ,high(v));
	for i:=0 to high(v) do
	  write(v[i]:8:2); end;


// В параметре нельзя объявить тип: открытый многомерный массив
// Поэтому опишем тип Matrix заранее.
procedure print_matrix(const m: Matrix);
var i,j: word;
begin
	writeln('print_vector. Мин. и макс. индексы матрицы: ', low(m),', ' ,high(m));
	writeln('print_vector. Мин. и макс. индексы матрицы: ', low(m[0]),', ' ,high(m[0]));
	// for i:=0 to high(m) do
		// for j:=0 to high(m[i])
	  // write(v[i]:8); 
	  end;

var
 vr: array [1..N] of Real;
 v1: array of Real;					// Динамический массив

 m1: array [1..N, 1..N] of Real;
 // Два следующих типа одинаковы
 m2: array of array of Real;	
 m3: Matrix;

 // v0: array [0..0] of Real;
 i: word;

begin
randomize;

writeln;
writeln('Мин. и макс. индексы массива vr: ',low(vr), ', ',high(vr));
// writeln('Мин. и макс. индексы массива vr: ',high(v0), ', ',low(v0));

SetLength(v1, N);	// Задаёт длинну динамического массива.
SetLength(m2, N, 2);	// Задаёт ращмерность динамического двумерного массива: число строк, число столбцов

for i:=1 to N do begin
	vr[i] := random*100-50;
	v1[i-1] := random*100-50;	// Индексация динамического массива начинается с  0
		end;

writeln;
print_vector(vr); writeln;
print_vector(v1); writeln;

writeln;
writeln('print_vector. Мин. и макс. индексы матрицы: ', low(m1),', ' ,high(m1));
for i:=1 to high(m1) do
writeln('print_vector. Мин. и макс. индексы строки матрицы: ',i, ', ', low(m1[i]),', ' ,high(m1[i]));

	// print_matrix(m2);

end.