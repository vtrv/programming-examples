program deijkstra_cycle;
{Программа бросает 2 кости пока не выпадут две шестёрки.
Алгоритм реализован с помощью цикла Дейкстры}

const n6 = 6;
var d1, d2: byte;


// Реализация цикла Дейкстры через оператор cintinue
procedure C1(var d1, d2:byte);
begin
repeat
  if (d1=6) and (d2<6) then 
    begin write(d1:2,' +',d2:2, ';  '); d2:=random(n6)+1; continue; end;
  if (d2=6) and (d1<6) then
    begin write(d1:2,' +',d2:2, ';  '); d1:=random(n6)+1; continue; end;
  if d1+d2<12 then 
    begin write(d1:2,' +',d2:2, ';  ');
    d1:=random(n6)+1; d2:=random(n6)+1; continue; end;
  break;
until False;
write(d1:2,' +',d2:2, ';  ');
end;


// Реализация цикла Дейкстры через оператор cintinue
procedure C2(var d1, d2:byte);
begin
repeat
  if (d1=6) and (d2<6) then 
    begin write(d1:2,' +',d2:2, ';  '); d2:=random(n6)+1 end
  else if (d2=6) and (d1<6) then
    begin write(d1:2,' +',d2:2, ';  '); d1:=random(n6)+1 end
  else if d1+d2<12 then 
    begin write(d1:2,' +',d2:2, ';  ');
    d1:=random(n6)+1; d2:=random(n6)+1 end
  else break;
until False;
write(d1:2,' +',d2:2, ';  ');

end;


begin

randomize;
d1 := random(n6)+1;
d2 := random(n6)+1;

write(d1:2,' +',d2:2, ';  ');

C1(d1, d2);
writeln('----------');
d1 := random(n6)+1; d2 := random(n6)+1;
C2(d1, d2);
writeln;
end.
