program variables;
type 
	// Перечисления можно объявлять в разделе описания типов
	Direction = (North, East, South, West); 

var
//  Порядковые типы. 
// http://freepascal.org/docs-html/ref/refsu5.html
i: integer;
c: char;
by: byte;
b: boolean;
dir: Direction; // Перечисление 
// Можно описать тип после объявления переменной.
// Значения переменной типа Перечисление может быть только идентификатором.
// Цифры не являются корректным идентификатором.
dir2: (N, E, S, W);  

month: 1..12;

x,y: Real;

dirs: array [0..16] of Direction;

begin
	randomize; 

	// Максимальные значения некоторых типов хранятся в константах стандартной библиотеки
	writeln('Максимальное значение типа integer    :', maxint:12);
	// Но это значение можно получить и так
	writeln('Максимальное значение типа integer    :', High(integer):12);
	writeln('Максимальное значение типа Longint    :', High(Longint):12);

	// Однако с вещественными типами это не работает
	//writeln('Максимальное значение типа Real        : ', High(Real):7); // <- ошибка компиляции

	i := maxint+1;		// Переполнение! Теперь в i записано минмальное значение для типа integer
	writeln('Максимальное значение типа integer +1 :', i:12);

	writeln('Старший и младший байты i=maxint   ( 32767):', Hi(maxint):4, Lo(maxint):4);
	writeln('Старший и младший байты i=maxint+1 (-32768):', Hi(i):4, Lo(i):4);

	writeln; writeln('Массив перечислений:');
	for i:=0 to High(dirs) do begin
		dirs[i]:=Direction(random(4));
		write(dirs[i]:6);     end;

	writeln;

	month := 12; writeln(month);
	month := 13; writeln(month);

end.