program proc_exmpl;
{ Примеры использования процедур и функций. }

// Глобальные постоянные
const N = 255;

type
  Func = function(x:real):real; // тип-функция

  FVector = array [1..N] of Real;
  IVector = array [1..N] of Integer;


// пример функции
// все формальные парамеры - параметры-значения
function datetostr(y:integer; m,d:byte): string;
// Конвертирует даты в строку
var s, ss:string; // Локальные переменные
begin
  str(y,ss);
  S:=ss+'.';
  str(m:2,ss);
  s:=s+ss+'.';
  str(d:2,ss);
  datetostr := s+ss;
end;


// Передача переменной по ссылке (параметр-переменная)
// Переменные X и Y передаются по ссылке.
// S передаётся по значению, т.е. в памяти процедуры будет создана копия этой переменной
procedure A(var x,y:real; s:string);
begin
// Формальным параметрам-переменным (переданным по ссылке) можно присваивать значения,
// это повлечёт изменение значений фактических параметров
x := x * (-1);
y := y * (-1);

// Локальная переменная S затеняет глобальную S. 
// Здесь изменится только локальная переменная.
s := 'qwerty'; 
writeln()
end;


// Пример функции без параметров
function F:real;
begin
F:=10;
end;


// Пример функции с параметром-константой.
// Вместо копирования значения параметра во временную память для передачи параметра в функцию
// передаётся ссылка на значение, которое однако нельзя менять.
procedure foo_c(const s:string);
begin
// s:= 'qwerty'; // Компилятор не допустит этого
writeln(s);
end;

// Перегрузка процедур(функций)
procedure foo_c;
  begin
  writeln('Это перегруженная процедура'); end;


// Можно описать процедуру с пустым телом
procedure bar;
begin end;


// подпрограмму можно описывать в двнутри тругой подпрограммы.
// Процедура С описана внутри B.
procedure B;
 procedure C;
  begin write('C'); end;
  begin write('B'); C; end;


// Пример рекурсии
procedure C13(i:integer);
begin
 write(i:4);
 if i<13 then inc(i)
 else exit;
 C13(i);
end;


// Передача открытого массива (массива произвольной длинны)
procedure print_vector(vector: array of byte);
  var i,n: integer;
  begin
  writeln('Array length ', high(vector)+1);
  n := high(vector);  // n - не длинна массива, а последний индекс. Длинна = n + 1
  // Индексация открытого массива начинается с 0
  for i:=0 to n do writeln(vector[i]:3);
  end;

// Перегрузка процедур(функций)
procedure print_vector(const v: FVector; n: integer);
  var i: integer;
  begin
  for i:=1 to n do write(v[i]:8:2,' ');
  end;

procedure print_vector(const v: IVector; n: integer);
  var i: integer;
  begin
  for i:=1 to n do write(v[i]:8,' ');
  end;


// Объявим функцию совпадающию с типом Func
function func_x2(x:real): real;
  begin
    func_x2 := x*x; 
  end;


// Функции передаётся функция
// Последний параметр со значением по умолчанию.
// Для компиляции FPC нужно передать компилятору ключ -S2
procedure print_func(f:Func; a,b: real; n:integer=10);
  const l = 10; m = 2;
  var i: integer;
      h: Real;
  begin
    h := (b-a)/(n-1);
    write('x:   ');
    for i:=0 to n-1 do  
      write(a+i*h :l:m);

    writeln; write('f(x):');
    for i:=0 to n-1 do
      write(f(a+i*h) :l:m);
  writeln(N);
  end;


// Х - нетипизированый параметр функции
procedure D(var x);
  var
    v: array [0..255] of Real absolute x; // Будем считать переменную Х массивом
    i: integer;
  begin
    for i:=0 to 255 do
    write(v[i]:4:1);
  end;


function F3:string;
begin
exit('exit');
  end;

// Глобальные переменные.
var 
 x,y:real;
 c: integer;
 s: string;
 v: array [1..N] of byte;
 v2: array [10..20] of byte;
 v3: array [byte] of Real;
 i: integer;
 f1, f2: Func;
 ff: array [1..3] of Func;

 vf: FVector;
 vi: IVector;


begin

  f1 := @func_x2;

//Пример вызова перегруженной процедуры
foo_c('Вызовется процедура с параметром');
foo_c;

// Пример вызова функции. Здесь фактические параметры-значения.
writeln(DateToStr(2017,01,05));
x:=2;
y:=3;
c:=4;
s:='1234';

// В x и y передаются по ссылке (см. описание профедуры A). 
A(x,y,s);
//A(3.14, 2.8 ,s); // Ошибка: По ссылке могу передаваться только переменные

writeln('x = ',x:4:1, '; y = ',y:4:1, ' ',s,' ',F);

writeln;
B;
writeln;
C13(0);
writeln;


// Фактический параметр может быть выражением, 
// если функция не требует передавать переменную по ссылке.
func_x2(2+3);


for i:=1 to N do
  v[i]:=random(256);

// Передача массива в процедуру
print_vector(v);
writeln(high(v));

writeln('---------------------');
f1 := @func_x2;
print_func(f1, 0, 9, 10);
writeln;

func_x2(10);


for i:=0 to high(v3) do
  v3[i]:=random;

// Вызов процедуры с нетипизированым параметром
D(v3);
writeln;


for i:=1 to 10 do begin
  vf[i]:=random;
  vi[i]:=random(32567); end;

//  Вызов перегруженных процедур
print_vector(vf,10); writeln;
print_vector(vi,10);
writeln;

writeln(F3);
end.
