program untyped_files;
{
  Пример работы с нетипизироваными файлами.
  Файлы можно открывать для чтения или записи, но не для добавления.
  Чтобы сделать последнее придётся заново копировать весь файл и дописать
  содержимое.
}
uses
  sysutils;

type
  Array32 = array [0..31] of byte;
const
  fn1 = 'typed-files-example.exe';  // исполняемый файл Windows
  fn2 = 'typed-files-example';		  // исполняемый файл Linux
  fn3 = 'untyped-files-example.pas';
  bsize = 8;

var 
 f: file;	// Нетипизированный файл
 filename: string;
 buf8: array [0..bsize-1] of byte;
 buf32: Array32;
 r,i: word;
 s: string;


// Печатает по байту значения массива в шестнадцатеричном виде
// номера блоков block_n начинаются с 0
procedure print_block(const block: array of byte; bsize: word);
  var i: word;
begin
  for i:=0 to bsize-1 do
     write(IntToHex(block[i],2):3);
end;


begin
  if FileExists(fn1) then filename:=fn1
  else if FileExists(fn2) then filename:=fn2
  else begin writeln('Файлы не найдены'); exit; end;

  assign(f,filename);
  reset(f,bsize);
  writeln('Размер файла "',filename,'" ',fileSize(f),' байт'); 
  // Прочитаем файл по одному блоку
  // Указатель на текущую позицию в файле будет сдвигаться на один блок за каждое чтение
  writeln('Первые 8 блоков по ',bsize,' байт: ');
  for i:=0 to 7 do
  begin
    blockread(f,buf8,1,r);
    print_block(buf8, bsize); writeln;
    end;

  writeln; writeln('Содержимое файла со смещением +2 блока:');
  seek(f,2);                // Функция seek всегда смещает текущую позицию файла от НАЧАЛА файла вперёд
  blockread(f,buf8, 1, r);
  print_block(buf8, bsize);

  writeln; writeln('следующим будет обрабатываться блок файла со смещением +',FilePos(f),': ');
  blockread(f,buf8, 1, r);
  print_block(buf8, bsize);
  close(f);
  writeln;
end.

